﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PayComputeApp.Services
{
    public interface INationalInsuranceContributionService
    {
        decimal NIContribution(decimal totalAmount);
    }
}

﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PayComputeApp.Persistence
{
    public static class DataSeedingInitializer
    {
        public static async Task UserAndRoleSeedAsync(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            string[] roles = { "Admin", "Manager", "Staff" };
            foreach (var role in roles)
            {
                var roleExist = await roleManager.RoleExistsAsync(role);
                if (!roleExist)
                {
                    IdentityResult result = await roleManager.CreateAsync(new IdentityRole(role));
                }
            }

            //Create Admin User
            if (userManager.FindByEmailAsync("ubongsky1@gmail.com").Result == null)
            {
                IdentityUser user = new IdentityUser
                {
                    UserName = "ubongsky1@gmail.com",
                    Email = "ubongsky1@gmail.com"
                };
                IdentityResult identityResult = userManager.CreateAsync(user, "petroleum11@").Result;
                if (identityResult.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Admin").Wait();
                }
            }

            //Create Manager User
            if (userManager.FindByEmailAsync("ubsky1@gmail.com").Result == null)
            {
                IdentityUser user = new IdentityUser
                {
                    UserName = "ubsky1@gmail.com",
                    Email = "ubsky1@gmail.com"
                };
                IdentityResult identityResult = userManager.CreateAsync(user, "petroleum11@").Result;
                if (identityResult.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Manager").Wait();
                }
            }

            //Create Staff User
            if (userManager.FindByEmailAsync("narth@gmail.com").Result == null)
            {
                IdentityUser user = new IdentityUser
                {
                    UserName = "narth@gmail.com",
                    Email = "narth@gmail.com"
                };
                IdentityResult identityResult = userManager.CreateAsync(user, "Password1").Result;
                if (identityResult.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Staff").Wait();
                }
            }

            //Create No Role User
            if (userManager.FindByEmailAsync("john.doe@gmail.com").Result == null)
            {
                IdentityUser user = new IdentityUser
                {
                    UserName = "john.doe@gmail.com",
                    Email = "john.doe@gmail.com"
                };
                IdentityResult identityResult = userManager.CreateAsync(user, "petroleum11@").Result;
                //No Role assigned to Mr John Doe
            }
        }
    }
}

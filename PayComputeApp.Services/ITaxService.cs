﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PayComputeApp.Services
{
    public interface ITaxService
    {
        decimal TaxAmount(decimal totalAmount);
    }
}

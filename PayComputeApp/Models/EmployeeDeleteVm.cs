﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PayComputeApp.Models
{
    public class EmployeeDeleteVm
    {
        public int Id { get; set; }
        public string FullName { get; set; }
    }
}
